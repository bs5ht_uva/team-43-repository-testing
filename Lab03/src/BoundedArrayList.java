import java.util.ArrayList;


public class BoundedArrayList extends ArrayList<String> {
private int maxSize;
	public BoundedArrayList(int maxSize){
		super(maxSize);
		this.maxSize=maxSize;
	}
	
	
	
	public boolean add(String s){
		if(this.size()==maxSize)
			return false;
		else{
			super.add(s);
			return true;
		
	}
}
}
