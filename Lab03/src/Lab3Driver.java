import java.util.ArrayList;

public class Lab3Driver {

	public static void main(String[] args) {
		
		ArrayList<String> list1 = new ArrayList<String>();
		UniqueArrayList list2 = new UniqueArrayList();
		BoundedArrayList list3 = new BoundedArrayList(4);
		
		ArrayList<String> listAlias = list2;  // change this to test each kind of list
		
		String[] hamlet = { "to", "be", "or", "not", "to", "be" };
		for (int i=0; i < hamlet.length; ++i) {
			listAlias.add( hamlet[i] );
		}
		System.out.println(listAlias);

	}
//add(index i, Object o) needs to be modified to check if there's a same object first
//same for addAll(addAll(Collection<? extends E> c))
//addAll(int index, Collection<? extends E> c)
}